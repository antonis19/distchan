package distchan

import (
	"bytes"
	"encoding/gob"
	"io"
	"log"
	"net"
	"os"
	"reflect"
	"sync"
)

// Client represents a registration between a network connection and a pair
// of channel slices, one for input and one for output.
type Client struct {
	conn               net.Conn
	inputChannels      []interface{}
	outputChannels     []interface{}
	inputChannelsMap   map[reflect.Type]int // map from datatype to index in inputChannels
	outputChannelsMap  map[reflect.Type]int // map from datatype to index in outputChannels
	signature          int32
	writeMutex         sync.RWMutex
	encoders, decoders []Transformer
	started            bool
	logger             *log.Logger
	waitGroup          *sync.WaitGroup
}

// NewClient creates a new Client instance
func NewClient(conn net.Conn, schan SessionChannelInterface) (*Client, error) {
	c := &Client{
		conn:              conn,
		inputChannels:     schan.GetInputChannels(),
		outputChannels:    schan.GetOutputChannels(),
		inputChannelsMap:  make(map[reflect.Type]int),
		outputChannelsMap: make(map[reflect.Type]int),
		signature:         defaultSignature,
		logger:            log.New(os.Stdout, "[distchan] ", log.Lshortfile),
		waitGroup:         new(sync.WaitGroup),
	}
	buildChannelsMap(c.inputChannels, c.inputChannelsMap)
	buildChannelsMap(c.outputChannels, c.outputChannelsMap)
	registerTypes(c.inputChannels)
	registerTypes(c.outputChannels)
	return c, nil
}

// Start instructs the client to begin serving messages.
func (c *Client) Start() *Client {
	c.waitGroup.Add(1)
	go c.handleIncomingMessages()
	c.waitGroup.Add(1)
	go c.handleOutgoingMessages()
	c.started = true
	return c
}

// handle messages coming from the network connection
func (c *Client) handleIncomingMessages() {
	var (
		buf                bytes.Buffer
		dec                = gob.NewDecoder(&buf)
		datatypeWaitGroups = make([]sync.WaitGroup, len(c.inputChannels))
	)

	for {
		buf.Reset()
		if err := readChunk(&buf, c.conn, c.signature); err != nil {
			if err != io.EOF {
				c.logger.Printf("%s (dropping connection)", err)
			}
			c.waitGroup.Done()
			return
		}
		runTransformers(&buf, c.decoders)
		var recvdInterface interface{}
		if err := dec.Decode(&recvdInterface); err != nil {
			if err == io.EOF {
				c.logger.Println("Unexpected EOF")
				c.waitGroup.Done()
				return
			}
			panic(err)
		}
		var channelIndex = -1
		channelIndex = findChannelIndex(recvdInterface, c.inputChannelsMap)

		if channelIndex == -1 { // datatype not found anywhere
			c.logger.Panic("no channel found for received datatype")
		}
		// Must wait until all messages of this datatype have been received
		// by the user program
		datatypeWaitGroups[channelIndex].Wait()
		datatypeWaitGroups[channelIndex].Add(1)
		go func(wg *sync.WaitGroup) {
			reflect.ValueOf(c.inputChannels[channelIndex]).Send(reflect.ValueOf(recvdInterface))
			wg.Done()
		}(&datatypeWaitGroups[channelIndex])
	}
}

// handle messages coming from user program and send them to
// the network connection
func (c *Client) handleOutgoingMessages() {
	var wg = new(sync.WaitGroup)
	for _, outputChannel := range c.outputChannels {
		wg.Add(1)
		go c.sendRoutine(outputChannel, wg)
	}
	wg.Wait()
	c.waitGroup.Done()
}

// sendRoutine intercepts outgoing messages from a channel in the user program
// and sends them through the network
func (c *Client) sendRoutine(channel interface{}, wg *sync.WaitGroup) {
	var (
		buf bytes.Buffer
		enc = gob.NewEncoder(&buf)
	)

	for {
		buf.Reset()

		x, ok := reflect.ValueOf(channel).Recv()
		if !ok { // channel was closed and drained
			wg.Done()
			break
		}
		var iface interface{} = x.Interface()
		if err := enc.Encode(&iface); err != nil {
			c.logger.Panicln(err)
		}

		runTransformers(&buf, c.encoders)
		if err := c.writeChunk(c.conn, &buf, c.signature); err != nil {
			c.logger.Printf("error writing value to connection: %s\n", err)
		}
	}

}

func (c *Client) writeChunk(w io.Writer, buf *bytes.Buffer, signature int32) error {
	c.writeMutex.Lock()
	defer c.writeMutex.Unlock()
	return writeChunk(w, buf, signature)
}

//

// AddEncoder adds a new encoder to the client. Any outbound messages
// will be passed through all registered encoders before being sent
// over the wire. See the tests for an example of encoding the data
// using AES encryption.
func (c *Client) AddEncoder(f Transformer) *Client {
	c.encoders = append(c.encoders, f)
	return c
}

// AddDecoder adds a new decoder to the client. Any inbound messages
// will be passed through all registered decoders before being sent
// to the channel. See the tests for an example of decoding the data
// using AES encryption.
func (c *Client) AddDecoder(f Transformer) *Client {
	c.decoders = append(c.decoders, f)
	return c
}

// Done should be called once all the channel operations in the user
// program have been performed.
func (c *Client) Done() {
	closeChannels(c.outputChannels)
	c.waitGroup.Wait()
}

// Logger exposes the client's internal logger so that it can be configured.
// For example, if you want the logs to go somewhere besides standard output
// (the default), you can use c.Logger().SetOutput(...).
func (c *Client) Logger() *log.Logger {
	return c.logger
}

// SetSignature sets the signature used by the client. The signature
// is the first value written to the connection on each message sent.
// When either side receives a new message, the first four bytes must
// match this signature or else the read is aborted.
//
// A sensibly-random value is provided by default, but this method
// can be used to enforce a custom signature instead.
func (c *Client) SetSignature(signature int32) {
	c.signature = signature
}
