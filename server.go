package distchan

import (
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"io"
	"log"
	"net"
	"os"
	"reflect"
	"runtime"
	"sync"
)

// Server represents a registration between a network listener and a pair
// of channel slices, one for input and one for output.
type Server struct {
	ln                    net.Listener
	inputChannels         []interface{}
	outputChannels        []interface{}
	inputChannelsMap      map[reflect.Type]int // map from datatype to index in inputChannels
	outputChannelsMap     map[reflect.Type]int // map from datatype to index in outputChannels
	signature             int32
	mu                    sync.RWMutex
	writeMutex            sync.RWMutex
	i                     int // current connection index
	conns                 []net.Conn
	encoders, decoders    []Transformer
	started, shuttingDown bool
	logger                *log.Logger
	waitGroup             *sync.WaitGroup
}

// NewServer registers a SessionChannelInterface with an active listener. Gob-encoded
// messages received on the listener will be passed to the appropriate input channel of the SessionChannel;
// any values passed to a SessionChannel's output channel will be gob-encoded and written to one open connection.
//
// Note that the returned value's Start() method must be called before any
// messages will be passed. This gives the user an opportunity to register
// encoders and decoders before any data passes over the network.
func NewServer(ln net.Listener, schan SessionChannelInterface) (*Server, error) {
	s := &Server{
		ln:                ln,
		inputChannels:     schan.GetInputChannels(),
		outputChannels:    schan.GetOutputChannels(),
		inputChannelsMap:  make(map[reflect.Type]int),
		outputChannelsMap: make(map[reflect.Type]int),
		signature:         defaultSignature,
		logger:            log.New(os.Stdout, "[distchan] ", log.Lshortfile),
		waitGroup:         new(sync.WaitGroup),
	}
	buildChannelsMap(s.inputChannels, s.inputChannelsMap)
	buildChannelsMap(s.outputChannels, s.outputChannelsMap)
	registerTypes(s.inputChannels)
	registerTypes(s.outputChannels)
	go s.handleIncomingConnections()
	return s, nil
}

// Start instructs the server to begin serving messages.
func (s *Server) Start() *Server {
	s.mu.Lock()
	defer s.mu.Unlock()
	for _, conn := range s.conns {
		s.waitGroup.Add(1)
		go s.handleIncomingMessages(conn)
	}

	s.waitGroup.Add(1)
	go s.handleOutgoingMessages()
	s.started = true
	return s
}

// handle incoming TCP connections from clients
func (s *Server) handleIncomingConnections() {
	defer s.shutdown()

	for {
		conn, err := s.ln.Accept()
		if err != nil {
			// for now, assume it's a "use of closed network connection" error
			s.shuttingDown = true
			return
		}
		s.addConn(conn)
		if s.started {
			s.waitGroup.Add(1)
			go s.handleIncomingMessages(conn)
		}
	}
}

// handle messages coming from the network connection
func (s *Server) handleIncomingMessages(conn net.Conn) {
	var (
		buf                bytes.Buffer
		dec                = gob.NewDecoder(&buf)
		datatypeWaitGroups = make([]sync.WaitGroup, len(s.inputChannels))
	)

	defer s.deleteConn(conn)

	for {
		buf.Reset()
		if err := readChunk(&buf, conn, s.signature); err != nil {
			if err != io.EOF {
				s.logger.Printf("%s (dropping connection)", err)
			}
			s.waitGroup.Done()
			return
		}
		runTransformers(&buf, s.decoders)
		var recvdInterface interface{}
		if err := dec.Decode(&recvdInterface); err != nil {
			if err == io.EOF {
				s.logger.Panic("Unexpected EOF")
				s.waitGroup.Done()
				return
			}
			panic(err)
		}

		if &recvdInterface == nil {
			panic("received nil interface")
		}

		var channelIndex = -1
		channelIndex = findChannelIndex(recvdInterface, s.inputChannelsMap)

		if channelIndex == -1 { // datatype not found anywhere
			s.logger.Panic("no channel found for received datatype")
		}
		// Must wait until all messages of this datatype have been received
		// by the user program
		datatypeWaitGroups[channelIndex].Wait()
		datatypeWaitGroups[channelIndex].Add(1)
		go func(wg *sync.WaitGroup) {
			reflect.ValueOf(s.inputChannels[channelIndex]).Send(reflect.ValueOf(recvdInterface))
			wg.Done()
		}(&datatypeWaitGroups[channelIndex])
	}
}

// handle messages coming from user program and send them to
// the network connection
func (s *Server) handleOutgoingMessages() {
	var (
		// Each connection needs a unique *gob.Encoder in order
		// to avoid "unknown type id" errors.
		gobbers = make(map[net.Conn]*gob.Encoder)
	)

	defer func() {
		if err := s.ln.Close(); err != nil {
			s.logger.Printf("error closing listener: %s\n", err)
		}
	}()

	var wg = new(sync.WaitGroup)
	for _, outputChannel := range s.outputChannels {
		wg.Add(1)
		go s.sendRoutine(outputChannel, gobbers, wg)
	}
	wg.Wait()
	s.waitGroup.Done()
}

// sendRoutine intercepts outgoing messages from a channel in the user program
// and sends them through the network
func (s *Server) sendRoutine(channel interface{}, gobbers map[net.Conn]*gob.Encoder, wg *sync.WaitGroup) {
	for {
		var buf bytes.Buffer
		buf.Reset()
		x, ok := reflect.ValueOf(channel).Recv()
		if !ok { // channel was closed and drained
			wg.Done()
			return
		}
		var iface = x.Interface()
		conn := s.currentConn()
		if conn == nil {
			s.WaitUntilReady()
			continue
		}
		gobbers[conn] = gob.NewEncoder(&buf)
		if err := gobbers[conn].Encode(&iface); err != nil {
			s.logger.Printf("%s (dropping connection and trying again)", err)
			s.deleteConn(conn)
			continue
		}
		runTransformers(&buf, s.encoders)
		if err := s.writeChunk(s.currentConn(), &buf, s.signature); err != nil {
			s.logger.Printf("%s (dropping connection and trying again)", err)
			s.deleteConn(s.currentConn())
		}
	}
}

func (s *Server) writeChunk(w io.Writer, buf *bytes.Buffer, signature int32) error {
	s.writeMutex.Lock()
	defer s.writeMutex.Unlock()
	return writeChunk(w, buf, signature)
}

// Shutdown the server
func (s *Server) shutdown() {
	s.mu.Lock()
	defer s.mu.Unlock()

	for _, conn := range s.conns {
		if err := binary.Write(conn, binary.LittleEndian, s.signature); err != nil {
			s.logger.Println(err)
		}
		if err := binary.Write(conn, binary.LittleEndian, int32(-1)); err != nil {
			s.logger.Println(err)
		}
	}
}

// Done should be called once all the channel operations in the user
// program have been performed.
func (s *Server) Done() {
	closeChannels(s.outputChannels)
	s.waitGroup.Wait()
}

//

// AddEncoder adds a new encoder to the server. Any outbound messages
// will be passed through all registered encoders before being sent
// over the wire. See the tests for an example of encoding the data
// using AES encryption.
func (s *Server) AddEncoder(f Transformer) *Server {
	s.encoders = append(s.encoders, f)
	return s
}

// AddDecoder adds a new decoder to the server. Any inbound messages
// will be passed through all registered decoders before being sent
// to the channel. See the tests for an example of decoding the data
// using AES encryption.
func (s *Server) AddDecoder(f Transformer) *Server {
	s.decoders = append(s.decoders, f)
	return s
}

// NumClient returns the number of clients connected to this server.
func (s *Server) NumClient() int {
	s.mu.RLock()
	defer s.mu.RUnlock()

	return len(s.conns)
}

// Ready returns true if there are any clients currently connected.
func (s *Server) Ready() bool {
	return s.NumClient() > 0
}

// WaitUntilReady blocks until the server has at least one client available.
func (s *Server) WaitUntilReady() {
	for {
		runtime.Gosched()
		if s.Ready() {
			s.i = 0
			return
		}
	}
}

// Logger exposes the server's internal logger so that it can be configured.
// For example, if you want the logs to go somewhere besides standard output
// (the default), you can use s.Logger().SetOutput(...).
func (s *Server) Logger() *log.Logger {
	return s.logger
}

// SetSignature sets the signature used by the server. The signature
// is the first value written to the connection on each message sent.
// When either side receives a new message, the first four bytes must
// match this signature or else the read is aborted.
//
// A sensibly-random value is provided by default, but this method
// can be used to enforce a custom signature instead.
func (s *Server) SetSignature(signature int32) {
	s.signature = signature
}

func (s *Server) addConn(conn net.Conn) {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.conns = append(s.conns, conn)
}

func (s *Server) currentConn() net.Conn {
	s.mu.RLock()
	defer s.mu.RUnlock()

	if len(s.conns) == 0 {
		return nil
	}
	if s.i >= len(s.conns) {
		s.i = 0
	}
	return s.conns[s.i]
}

func (s *Server) deleteConn(conn net.Conn) {
	s.mu.Lock()
	defer s.mu.Unlock()

	for i := range s.conns {
		if s.conns[i] == conn {
			s.conns = append(s.conns[:i], s.conns[i+1:]...)
			break
		}
	}
}

func (s *Server) nextConn() {
	s.mu.Lock()
	defer s.mu.Unlock()

	s.i++
	if s.i >= len(s.conns) {
		s.i = 0
	}
}
