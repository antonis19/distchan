package distchan

import (
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"errors"
	"io"
	"io/ioutil"
	"reflect"
)

const defaultSignature int32 = 0x7f38b034

var (
	errNotChannel       = errors.New("distchan: provided value is not a channel")
	errInvalidSignature = errors.New("distchan: invalid signature")
)

func readChunk(buf io.Writer, r io.Reader, signature int32) error {
	var n int32
	if err := binary.Read(r, binary.LittleEndian, &n); err != nil {
		return err
	}

	if n != signature {
		return errInvalidSignature
	}

	if err := binary.Read(r, binary.LittleEndian, &n); err != nil {
		return err
	}

	if n == -1 {
		return io.EOF
	}

	_, err := io.CopyN(buf, r, int64(n))
	return err
}

func writeChunk(w io.Writer, buf *bytes.Buffer, signature int32) error {
	if err := binary.Write(w, binary.LittleEndian, signature); err != nil {
		return err
	}
	if err := binary.Write(w, binary.LittleEndian, int32(buf.Len())); err != nil {
		return err
	}
	_, err := buf.WriteTo(w)
	return err
}

// Transformer represents a function that does an arbitrary transformation
// on a piece of data. It's used for defining custom encoders and decoders
// for modifying how data is sent across the wire.
type Transformer func(src io.Reader) io.Reader

func runTransformers(buf *bytes.Buffer, transformers []Transformer) {
	if len(transformers) == 0 {
		return
	}
	var r io.Reader = buf
	for _, f := range transformers {
		r = f(r)
	}
	b, err := ioutil.ReadAll(r)
	if err != nil {
		panic(err)
	}
	buf.Reset()
	if _, err := buf.Write(b); err != nil {
		panic(err)
	}
}

// A SessionChannel should have a slice of input channels,
// and a slice of output channels.
type SessionChannelInterface interface {
	GetInputChannels() []interface{}
	GetOutputChannels() []interface{}
}

// This function closes each of the channels of the given slice
func closeChannels(channels []interface{}) {
	for _, channel := range channels {
		reflect.ValueOf(channel).Close()
	}
}

// Build a map of key-value pairs of type (datatype,channelIndex)
func buildChannelsMap(channels []interface{}, channelsMap map[reflect.Type]int) {
	for i, channel := range channels {
		chanType := reflect.TypeOf(channel)
		elem := chanType.Elem() // <--
		channelsMap[elem] = i
	}
}

// Register the datatypes with the gob package for encoding and decoding.
func registerTypes(channels []interface{}) {
	for _, channel := range channels {
		chanType := reflect.TypeOf(channel)
		elem := chanType.Elem()
		gob.Register(reflect.New(elem).Elem().Interface())
	}
}

// Find the index of of the channel corresponding to the given datatype
func findChannelIndex(iface interface{}, channelsMap map[reflect.Type]int) (channelIndex int) {
	datatype := reflect.TypeOf(iface)
	channelIndex, ok := channelsMap[datatype]
	if !ok {
		channelIndex = -1
	}
	return channelIndex
}
