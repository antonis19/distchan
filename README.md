# distchan
This version of the distchan package is built upon the original [distchan](https://github.com/dradtke/distchan) package for distributed Go channels.

The features added to the original distchan package include:

- statically typed channel communication through the use of a pair of channel slices of different datatypes.

- ```gob``` encoding of messages supports custom datatypes also.

- ```Client``` and ```Server``` terminate gracefully so that all pending messages are sent.

## How to install
Type the following command into your terminal:

```go get -u bitbucket.org/antonis19/distchan```


## How to use

To import this package into your program use the following line:

```go
import "bitbucket.org/antonis19/distchan"
```
Refer to the documentation in the original [distchan](https://github.com/dradtke/distchan)
for usage, keeping in mind the following changes:

- Unlike the original distchan wherein ```NewClient``` and ```NewServer``` expect an input channel and an output channel, in this version these two functions expect a ```SessionChannelInterface``` which implements two methods: ```GetInputChannels()``` and ```GetOutputChannels()``` each returning a **slice** of channels.

- When the channel code in the user program is finished the ```Done()``` method should be called on ```Server```/```Client``` to terminate distchan gracefully.

